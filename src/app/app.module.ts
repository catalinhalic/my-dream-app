import { ModuleSampleModule } from './module-sample/module-sample.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatalinDeveloperComponent } from './catalin-developer/catalin-developer.component';
import { ExercitiiOneComponent } from './exercitii-one/exercitii-one.component';
import { AddEditDogComponent } from './add-edit-dog/add-edit-dog.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CatalinDeveloperComponent,
    ExercitiiOneComponent,
    AddEditDogComponent,
  ],
  // FormsModule, ReactiveFormsModule => pentru a putea folosi formularele
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ModuleSampleModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
