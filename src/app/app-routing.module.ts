import { AddEditDogComponent } from './add-edit-dog/add-edit-dog.component';
import { CatalinDeveloperComponent } from './catalin-developer/catalin-developer.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsExamComponent } from './module-sample/forms-exam/forms-exam.component';

const routes: Routes = [
  {
    path: 'catalin/:id',
    component: CatalinDeveloperComponent,
  },
  {
    path: 'forms-exam-page',
    component: FormsExamComponent,
  },
  {
    path: '',
    component: AddEditDogComponent,
  },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
