import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalin-developer',
  templateUrl: './catalin-developer.component.html',
  styleUrls: ['./catalin-developer.component.css'],
})
export class CatalinDeveloperComponent {
  // variabilele de clasa nu au nevoie de let, var, const
  title: string = 'Test';
  names: Array<string> = ['Ana', 'Maria', 'Ioana', 'Mihaela'];

  constructor() {
    // var, let, const
    let message: string = this.printMessage('Welcome to Angular World!');

    console.log('Log Constructor');
    console.log(message);
  }

  public printMessage(message: string): string {
    console.log('message from method');

    console.log(message);

    return message;
  }

  public showAlert() {
    alert('Bun venit');
  }
}
