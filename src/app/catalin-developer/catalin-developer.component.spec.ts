import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalinDeveloperComponent } from './catalin-developer.component';

describe('CatalinDeveloperComponent', () => {
  let component: CatalinDeveloperComponent;
  let fixture: ComponentFixture<CatalinDeveloperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatalinDeveloperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalinDeveloperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
