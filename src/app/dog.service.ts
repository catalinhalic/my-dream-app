import { Injectable } from '@angular/core';
import { Dog } from './exercitii-one/exercitii-one.component';

@Injectable({
  providedIn: 'root',
})
export class DogService {
  private doggies: Array<Dog> = [{ name: 'Lord', age: '2' }];

  constructor() {}

  public fetchDogs(): Array<Dog> {
    return this.doggies;
  }

  public addDog(dog: Dog): void {
    this.doggies.push(dog);
  }

  // public sendForm(form) {
  //   http.post("http://www.domeniu.com/adaugare-formular", form);
  // }
}
