import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsExamComponent } from './forms-exam.component';

describe('FormsExamComponent', () => {
  let component: FormsExamComponent;
  let fixture: ComponentFixture<FormsExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
