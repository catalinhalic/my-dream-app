import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forms-exam',
  templateUrl: './forms-exam.component.html',
  styleUrls: ['./forms-exam.component.css'],
})
export class FormsExamComponent implements OnInit {
  formData: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private router: Router
  ) {
    this.formData = formBuilder.group({
      email: [
        '',
        Validators.compose([Validators.email, Validators.minLength(2)]),
      ],
      password: [''],
    });
  }

  ngOnInit(): void {}

  sendForm() {
    if (this.formData.valid) {
      // console.log(this.formData.value);

      this.httpClient
        .post(
          'https://api.codebyte-software.com:9000/auth/login',
          this.formData.value
        )
        .subscribe(
          (response) => {
            console.log(response);
          },
          (error) => {
            console.log(error.message);
          }
        );

      // this.router.navigateByUrl('/catalin');
      let id = 213131232;

      this.router.navigate(['/', 'catalin', id]);
    } else {
      console.log('Formularul este invalid!');
    }
  }
}
