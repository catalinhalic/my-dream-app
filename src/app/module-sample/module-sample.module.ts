import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LifecyclesComponent } from './lifecycles/lifecycles.component';
import { FormsExamComponent } from './forms-exam/forms-exam.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [FormsExamComponent, LifecyclesComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [FormsExamComponent, LifecyclesComponent],
})
export class ModuleSampleModule {}
