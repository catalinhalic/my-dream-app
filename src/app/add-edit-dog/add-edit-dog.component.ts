import { DogService } from './../dog.service';
import { Component, Input, OnInit } from '@angular/core';
import { Dog } from '../exercitii-one/exercitii-one.component';

@Component({
  selector: 'app-add-edit-dog',
  templateUrl: './add-edit-dog.component.html',
  styleUrls: ['./add-edit-dog.component.css'],
})
export class AddEditDogComponent implements OnInit {
  @Input() dogName: string = '';

  constructor(private dogService: DogService) {}

  ngOnInit(): void {}

  saveDog(): void {
    let dog: Dog = { name: this.dogName };

    console.log(dog);

    this.dogService.addDog(dog);
  }

  updateName(event: any) {
    console.log(event);
  }

  updateAge(event: any) {
    console.log(event);
  }
}
