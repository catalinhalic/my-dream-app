import { DogService } from './../dog.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-exercitii-one',
  templateUrl: './exercitii-one.component.html',
  styleUrls: ['./exercitii-one.component.css'],
})
export class ExercitiiOneComponent implements OnInit {
  doggies: Array<Dog> = [];

  selectedDog: Dog = { name: 'rex', age: '2' };

  constructor(private dogService: DogService) {
    console.log('Constructor Code Block!');
  }

  ngOnInit(): void {
    // this.dogService.fetchDogs()
    // returneaza o lista de obiecte de tipul dog

    // ngOnInit se apeleaza o singura data in lifecycle-ul componentei
    this.doggies = this.dogService.fetchDogs();
  }

  selectDog(dog: Dog): void {
    console.log(dog);

    this.selectedDog = dog;
  }

  showAlert(): void {
    alert('Woofs!');
  }

  addDog(): void {
    let dog: Dog = { name: 'Rex' };

    this.dogService.addDog(dog);

    this.doggies = this.dogService.fetchDogs();
  }
}
export interface Dog {
  name: string;
  age?: string;
}
