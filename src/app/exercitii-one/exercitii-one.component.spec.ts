import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExercitiiOneComponent } from './exercitii-one.component';

describe('ExercitiiOneComponent', () => {
  let component: ExercitiiOneComponent;
  let fixture: ComponentFixture<ExercitiiOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExercitiiOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExercitiiOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
