import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html', //./ inseamna ca sunt in acelasi folder
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'my-dream-app';
}
